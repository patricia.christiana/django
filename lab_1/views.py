from django.shortcuts import render
from datetime import *

# Enter your name here
mhs_name = 'Patricia Christiana' # TODO Implement this

# birth year
mhs_birth = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(mhs_birth)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    current_year = datetime.now().year
    current_age = current_year - birth_year
    return current_age
